<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <?php
          function login() {
              $authentification = '';
              if (isset($_GET['firstname']) AND isset($_GET['username'])) {
                  $authentification = 'Bonjour <strong>' . $_GET['firstname'] . '</strong>, vous êtes authentifié en tant que <strong>' . $_GET['username'] . '</strong>.<br/>' .
                                      'Vous n\'êtes pas <strong>' . $_GET['firstname'] . '</strong>? Revenez au <a href="index.php">formulaire</a> pour corriger cela.';
              }
              else {
                  $authentification = '<em>Merci de renseigner les informations demandées. C\'est par <a href="index.php">ici</a>.</em>';
              }
              return $authentification;
          }
          echo login();
      ?>
  </body>
</html>
