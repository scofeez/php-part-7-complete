<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exo 5 x Foundation6</title>
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/libs/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/libs/bootstrap-social/bootstrap-social.css">
    <link rel="stylesheet" href="assets/css/style.css">
  </head>
  <body>
      <div class="row scf-center">
          <div class="medium-7 medium-centered columns">
              <?php
                  function nfo() {
                      $information = '';
                      if (empty($_POST['lastname']) AND empty($_POST['adress'])) {
                          $information = '<div class="scf-nfo"><i class="fa fa-info" aria-hidden="true"></i> <span>Merci de renseigner votre ' . (empty($_POST['lastname']) ? '<strong>nom de famille</strong>' : '<strong>adresse postale</strong>') . '</span></div>';
                      }
                      else {
                          $information = false;
                      }
                      return $information;
                  }
                  echo nfo();
              ?>
              <form method="post" action="index.php">
                  <select name="civil">
                    <option name="Mr" value="Mr">Mr</option>
                    <option name="Mme" value="Mme">Mme</option>
                  </select>
                  <p>
                    <input type="text" name="lastname" value="" placeholder="Nom de famille">
                    <input type="text" name="adress" value="" placeholder="Adresse postale">
                    <input class="button scf-green" type="submit" name="submit" value="Envoyer les données">
                  </p>
              </form>
              <?php
                  function result() {
                      $authentification = '';
                      if ((isset($_POST['lastname']) AND !empty($_POST['lastname'])) && (isset($_POST['adress']) AND !empty($_POST['adress']))) {
                          $authentification = 'Bonjour <strong>' . $_POST['civil'] . ' ' . $_POST['lastname'] . '</strong>,<br/>nous allons enregistrer l\'adresse suivante: <strong><em>' . $_POST['adress'] . '</em></strong>.';
                      }
                      return $authentification;
                  }
                  echo result();
              ?>
          </div>
      </div>

      <script src="assets/libs/jquery/dist/jquery.js"></script>
      <script src="assets/libs/what-input/what-input.js"></script>
      <script src="assets/libs/foundation-sites/dist/foundation.js"></script>
      <script src="js/app.js"></script>
  </body>
</html>
