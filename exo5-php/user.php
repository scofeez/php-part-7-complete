<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <?php
          function login() {
              $authentification = '';
              if (isset($_POST['firstname']) AND isset($_POST['username'])) {
                  $authentification = 'Bonjour <strong>' . $_POST['firstname'] . '</strong>, vous êtes authentifié en tant que <strong>' . $_POST['username'] . '</strong>.<br/>' .
                                      'Vous n\'êtes pas <strong>' . $_POST['firstname'] . '</strong>? Revenez au <a href="index.php">formulaire</a> pour corriger cela.';
              }
              else {
                  $authentification = '<em>Merci de renseigner les informations demandées. C\'est par <a href="index.php">ici</a>.</em>';
              }
              return $authentification;
          }
          echo login();
      ?>
  </body>
</html>
