var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

var sassPaths = [
  'assets/libs/foundation-sites/scss',
  'assets/libs/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src(['assets/scss/app.scss', 'assets/scss/style.scss'])
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('default', ['sass'], function() {
  gulp.watch(['assets/scss/*.scss'], ['sass']);
});
